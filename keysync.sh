#!/usr/bin/env bash
set -euo pipefail

# Script to deploy the ssh key/s in the current repo to ~/.ssh.
# The same script is used in different ssh key repos I deploy
# on my machines; as such not all of the script is applicable to the
# current repo.

# Get name of the script
progname="$(basename "$0")"

# Get source dir of this script (https://stackoverflow.com/a/246128)
script_dir="$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null 2>&1 && pwd )"
repo_dir="$script_dir"

# control or workstation. "control", is like my primary workstation; it is the
# only machine which can ssh to all other hosts. Conversely, a 'workstation'
# machine can only ssh to control. A basic 'node' is just a server.
target_type="node"

cmd_usage () {

# -r = deploy keys to root also
# -a [username] = deploy keys to username instead of current user
# -t = target type. "control" "workstation" or "node" (default)

echo "$progname: Usage description here..."
exit 0
}

private_keys () {

	# This loop is only necessary/used in repos containing private keys
	# "for loop; done || true" construction is required to avoid error when 
	# the if statement fails
	for i in "$repo_dir"/*; do

		fullpath=""
		fullpath="$(echo "$i" | grep id_rsa | grep  -v \.pub)"
		filename="$(basename "$fullpath")"

		if ! [ -z "${fullpath}" ] && ! ls "$homedir/.ssh/$filename" > /dev/null 2>&1; then
      # If target_type is not control but the current file is the control2node key, do nothing
      if [[ $fullpath =~ ^.*\control2node.*\.id_rsa$ ]] && ! [ "$target_type" == 'control' ]; then echo bleh
      else
        echo "Copy $fullpath to $homedir/.ssh..."
        cp "$fullpath" "$homedir/.ssh"

        echo "Set $filename to 600 permissions..."
        chmod 600 "$homedir/.ssh/$filename"
      fi
		fi

	done || true
}

authorized_keys_deploy () {

	mkdir -p "$homedir/.ssh" || true

	# If authorized_keys is not yet deployed to $HOME/.ssh of the current
	# user (or root, if the "-r" option is passed), deploy it.
	if ! diff "$authkeys" "$homedir/.ssh/authorized_keys" > /dev/null 2>&1; then

		echo "Deploying authorized_keys to $homedir/.ssh/authorized_keys..."

		cp "$authkeys" "$homedir/.ssh"
		chmod 600 "$homedir/.ssh/authorized_keys"
	fi

    chown -R "$(id -u "$curruser"):$(id -g "$curruser")" "$homedir/.ssh"
}

# update this function to allow it to be called in isolation if the user wishes
# to refresh which keys are authorised.
authorized_keys_user_cycle () {

	authorized_keys_deploy

	if [ "$allowroot" == 'yes' ]; then curruser=0; homedir="$roothome"; fi

	authorized_keys_deploy

	# If "-r" option is not selected, make sure there are no authorized
	# keys for root user
	
	if [ "$allowroot" == 'no' ] && [ -d "$roothome/.ssh" ]; then
		echo "Removing .ssh dir in $roothome..."; rm -rf "$roothome/.ssh"; fi
}

sshd_conf_update () {

	# If password auth is set to 'yes' anywhere in sshd_config then update:
	if grep -Eq "^.*PasswordAuthentication\s*[Yy][Ee][Ss]\s*(#.*)?$" \
	/etc/ssh/sshd_config; then
	  echo "Update sshd_config to prohibit password authentication..."
	  sed -i -e "s/^.*PasswordAuthentication.*$/PasswordAuthentication no/g" \
		/etc/ssh/sshd_config
	fi

	# If "-r" option is passed and PermitRootLogin is set anywhere in
	# sshd_config then update:
	if [ "$allowroot" == 'yes' ] && grep -Eq \
	"^\s*PermitRootLogin\s*[Nn][Oo]\s*(#.*)?$" /etc/ssh/sshd_config; then
		echo 'Update sshd_config to allow root login...'; fi

	if [ "$allowroot" == 'no' ] && grep -Eq \
	"^\s*PermitRootLogin\s*[Yy][Ee][Ss]\s*(#.*)?$" /etc/ssh/sshd_config; then
		echo 'Update sshd_config to ensure root login prohibited...'; fi

	sed -i -e "s/^.*PermitRootLogin.*$/PermitRootLogin $allowroot/g" \
		/etc/ssh/sshd_config
}

main () {

  # Set defaults
  export allowroot='no'
  export curruser
  if [ -z ${SUDO_USER+x} ]; then curruser='root'; else curruser="$SUDO_USER"; fi

  # Create temp working dir
  workdir="$(mktemp -dt 'XXXXXXXXXX')"
  
  # Get the home directory of the user who called the script (works when script
  # is called via sudo

  while true; do
    case "$1" in
      -h | --help ) cmd_usage ;;
      -r ) allowroot='yes'; shift ;;
      -a ) if id "$2" &>/dev/null; then curruser="$2"
           else echo "$progname: '$2' no such user"; exit 1; fi; shift; shift ;;
      -t ) case "$2" in
           'control' | 'workstation' | 'node' ) target_type="$2" ;;
           *) echo "$progname: '$2' is an invalid target"; exit 1 ;; esac; shift; shift; ;;
      -- ) shift; break ;;
      * ) break ;;
    esac
  done

  # Set homedir after checking command options in case -a option was selected
  export homedir="$(eval echo "~$curruser")"

  sshd_conf_update # Make any updates to /etc/ssh/sshd_conf, if required

  # Deploy any private keys available in repo, if not plain node
  if ! [ "$target_type" == 'node' ]; then private_keys; fi

  # Deploy authorized_keys file, if present in repo
  case "$target_type" in
    'control' | 'workstation' )
      if [ -d "$repo_dir/authorized_keys.$target_type" ]; then
      cd "$repo_dir/authorized_keys.$target_type"
      cat ./* > "$workdir/authorized_keys"
      authkeys="$workdir/authorized_keys"
      authorized_keys_user_cycle; fi ;;
    'node' )
      if [ -f "$repo_dir/authorized_keys" ]; then
      authkeys="$repo_dir/authorized_keys"
      authorized_keys_user_cycle; fi ;;
  esac

  # Cleanup working files when exit signal is received
  trap "rm -rf $workdir" EXIT
}

# Need to set early, as this is used for the non-sudo program response
export roothome="$(eval echo "~$(id -un 0)")"

# Need to be root to run the sshd_config sed commands
# https://stackoverflow.com/a/18216122/13160663
if [ "$EUID" -ne 0 ]
  then echo "$progname: Please run as root - the script wants to modify /etc/ssh/sshd_config (and $roothome/.ssh if the -r flag is passed)"; exit 1; fi

if OPTS="$(getopt -n "$progname" -o hra:t: -l help -- "$@")"; then :
	else exit 1; fi

# Set arg values to getopt parsed output
eval set -- "$OPTS"

main "$@"
